﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowLevel : MonoBehaviour {

    GameManager Manager;

    public ControlPlayer player;
    private float waitTime;
    
    public GameObject canvas;

    [Header("Tiempo mostrando el movimiento enemigo (segundos)")]
    public float startWaitTime;

    //public float theTime;

    public bool SHOW;
    public bool stopShow;

    // Use this for initialization
    void Start () {

        Manager = GameManager.Instance;
        canvas = GameObject.FindWithTag("CronoCanvas");
        stopShow = false;

    }

     void Init()
    {
        SHOW = true;
        canvas.gameObject.SetActive(false);

        player = Manager.controlPlayer;
        
        player.gameObject.SetActive(false);
        Manager.SetShow(SHOW);
        waitTime = startWaitTime;

        player.gameObject.SetActive(false);
        //StartCoroutine(ShowCorrutine());
    }

    // Update is called once per frame
    void Update () {

        //Debug.Log(stopShow);

        //theTime = waitTime;
        if (SHOW)
        {
            if (stopShow)
            {
                canvas.gameObject.SetActive(true);
                player.gameObject.SetActive(true);
                waitTime = startWaitTime;
                SHOW = false;
                Manager.SetShow(SHOW);

            }
            
        }
    }

    public void setStopShow(bool _stopShow)
    {
        stopShow = _stopShow;
    }

    IEnumerator ShowCorrutine()
    {
        while (SHOW)
        {
            //theTime = waitTime;
            waitTime -= Time.deltaTime;

            yield return null;
        }
    }

}               /*
                if (waitTime <= 0){
                    waitTime = startWaitTime;
                    SHOW = false;
                }else
                    waitTime -= Time.deltaTime;
                
                */