﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;


public class ShowPanelManager : MonoBehaviour {

    // Use this for initialization

    [SerializeField] GameObject pressEnter;

    GameObject CameraGlitch;
    CameraFilterPack_FX_Glitch1 glitch;
    void Start () {

        CameraGlitch = GameObject.FindGameObjectWithTag("Camera2");
        glitch = CameraGlitch.GetComponent<CameraFilterPack_FX_Glitch1>();
    }
	
	// Update is called once per frame
	void Update () {
        pressEnter.GetComponent<TextMeshProUGUI>().color = Color.Lerp(Color.white, new Color(0,0,0,0), Mathf.PingPong(Time.time, 0.8f));
    }

    public void ClosedPanel()
    {
        StartCoroutine(Closed(glitch));

    }
    IEnumerator Closed(MonoBehaviour effect)
    {
        glitch.enabled = !glitch.enabled;
        yield return new WaitForSecondsRealtime(0.1f);
        effect.enabled = !effect.enabled;
        this.gameObject.SetActive(false);

    }
}
