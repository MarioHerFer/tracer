﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlPlayer : MonoBehaviour {

    Rigidbody rb;

    public bool UP, DOWN, RIGHT, LEFT;

    [SerializeField] float speed = 1.0f;
    float MaxLenght = 0f;//, angleRotation;

    Vector3 input, StartPos, lastPos;
    Quaternion vectorRotation, StartRot, lastRot;
    List<Vector3> ps = new List<Vector3>();
    List<Quaternion> rt = new List<Quaternion>();

    int psN;
    public bool rec;

    private float currentTime;
    public bool Back, ReBack, Restart;

    float moduloV;

    [SerializeField] Collider Meta;
    public bool RESETLEVEL = false;
    GameManager Manager;

    // Use this for initialization
    void Start(){
        psN = 0;
        rec = true;
        ReBack = false;
        Restart = false;
        StartPos = transform.position;
        StartRot = transform.rotation;
        rb = GetComponent<Rigidbody>();
        speed = speed / 2;

        Back = false;

        Manager = GameManager.Instance;

        Manager.controlPlayer = this;
    }
   
    void Update(){
        Back = Manager.GetBACK();

        //Debug.Log(psN);
        if (Manager.GetMenuPause())
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, 0);
            input.x = input.z = 0;
            Manager.SetPaused(true);
        }

        if (!Back){ 
            if (rec && !Manager.GetMenuPause())
                Rec();
            else if(!Restart && !Manager.GetMenuPause())
                Go();
        }
        if (ReBack){
            Manager.SetPaused(true);
            Manager.SetBACK(true);

            if (psN > 0) --psN;
            transform.position = (ps[psN]);
            transform.rotation = (rt[psN]);
            ps.Remove(ps[psN]);
            rt.Remove(rt[psN]);

            if (psN == 0)
            {
                transform.position = StartPos;
                transform.rotation = StartRot;

                psN = 0;
                ReBack = false;
                Restart = false;
                if (!rec) { 
                    rec = true;
                    Manager.SetLight(true);
                    Manager.SetDetected(false);
                }
                //Debug.Log("Reinicio");
                Manager.SetBACK(false);
                Manager.ResetCrono();
                ps = new List<Vector3>(); 
                rt = new List<Quaternion>();
            }
        }
        //Debug.Log(ps.Count);

    }

    void Rec(){
                
        UP = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
        DOWN = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
        RIGHT = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
        LEFT = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);

        //Move Diagonal
        if (UP && RIGHT)
        {
            input.z = speed * (Mathf.Sqrt(2) / 2);
            input.x = speed * (Mathf.Sqrt(2) / 2);
        }
        if (UP && LEFT)
        {
            input.z = speed * (Mathf.Sqrt(2) / 2);
            input.x = -speed * (Mathf.Sqrt(2) / 2);
        }
        if (DOWN && RIGHT)
        {
            input.z = -speed * (Mathf.Sqrt(2) / 2);
            input.x = speed * (Mathf.Sqrt(2) / 2);
        }
        if (DOWN && LEFT)
        {
            input.z = -speed * (Mathf.Sqrt(2) / 2);
            input.x = -speed * (Mathf.Sqrt(2) / 2);
        }

        //Move Hor/Ver
        if (UP && !RIGHT && !LEFT)
            input.z = speed;

        if (DOWN && !RIGHT && !LEFT)
            input.z = -speed;

        if (RIGHT && !UP && !DOWN)
            input.x = speed;

        if (LEFT && !UP && !DOWN)
            input.x = -speed;


        rb.AddForce(Vector3.right * 500 * input.x);
        rb.AddForce(Vector3.forward * 500 * input.z);
        
        rb.AddForce(Vector3.right * 500 * input.x);
        rb.AddForce(Vector3.forward * 500 * input.z);

        if (input.x != 0 && input.z != 0)
            MaxLenght = 1f;
        else
            MaxLenght = 0f;

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxLenght);
        //rb.velocity = new Vector2(Vector2.zero.x, Vector2.zero.y);


        //float angleRotation = Vector3.Angle(input,Vector3.forward);
        //Debug.Log(angleRotation);

        //transform.rotation = Quaternion.Euler(0, angle, 0);

        if (input.x != 0 || input.z != 0){
        vectorRotation = Quaternion.Euler(0f, Vector3.Angle(input, Vector3.forward), 0f);
            if (input.x < 0) 
                vectorRotation = Quaternion.Euler(0f, -Vector3.Angle(input, Vector3.forward), 0f);
        transform.rotation = Quaternion.Euler(Vector3.Lerp(transform.rotation.eulerAngles, vectorRotation.eulerAngles, 0.5f));
        }
        
        
        if (input.x != 0 || input.z != 0){
            ps.Add(transform.position);
            rt.Add(transform.rotation);
            psN++;
            Manager.SetPaused(false);
        }else
            Manager.SetPaused(true);
        
        
        input.x = input.z = 0;
    }
    void Go(){
        if (psN < ps.Count - 1) psN++;

        transform.position = (ps[psN]);
        transform.rotation = (rt[psN]);

        if (psN == ps.Count - 1){
            transform.position = lastPos;
            transform.rotation = lastRot;
            //Todo debe detenerse
        }
    }
    public void Remove(){

        if (rec){
            if (psN == 0)
            {
                transform.position = StartPos;
                transform.rotation = StartRot;
                return;
            }

            Manager.SetPaused(true);
            Manager.SetBACK(true);

            if (psN > 0) psN--;

            transform.position = (ps[psN]);
            transform.rotation = (rt[psN]);
            ps.Remove(ps[psN]);
            rt.Remove(rt[psN]);
            
        }
        
    }
    /*
    public void RestePos(){
        if (rec)
            ReBack = true;
        //Restart para cuando acabas Go()
    }
    */
    public void ResetStart() {
        ReBack = true;
        Restart = true;
        rec = false;
        
    }

    //inicia el trazado
    public void StartTracer(){
            lastPos = transform.position;
            lastRot = transform.rotation;

            transform.position = StartPos;
            transform.rotation = StartRot;
            rec = false;
            Manager.SetPaused(false);
            Manager.ResetCrono();
            psN = 0;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other == Meta)
        {

            Camera.main.GetComponent<effectManager>().theGlitch();

            if (rec)
            StartTracer();
            else{
                //Debug.Log("Win!");
                if (RESETLEVEL)
                    Manager.ResetScene();
                else
                    Manager.NextScene();
                //nivel superado
            }
        }
        
    }


    //IEnumerator RR()
    //{
    //    while (psN > 0)
    //    {
    //        psN--;
    //        transform.position = (ps[psN]);
    //        transform.rotation = (rt[psN]);
    //        ps.Remove(ps[psN]);
    //        rt.Remove(rt[psN]);
    //        yield return null;
    //    }
    //}
}




// Corrutina
/*
//Esto es una corutina. Es como hacer otro hilo de ejecución (trampeando, realmente no lo haces)
//Al hacer "yield return null" lo que haces es esperar al siguiente frame para continuar con la ejecución del código
//Por lo tanto, si quieres hacer un efecto de Degradado de Color, lo harías de ésta manera para facilitarte la vida
//La opinión de Victor es usarlas lo mínimo posible, ésto se podria hacer de otra manera, peeeero hacerlo con corutinas te facilita la vida
//Si quisieras hacerlo sin corrutinas lo harias sin un bucle for, simplemente poner el i++ fuera del bucle y acceder a esa posición en cada frame
IEnumerator MovePlayer() {
    for (i = 0; i < ps.Count; i++){
        transform.Translate(ps[i].x, ps[i].y, 0);
     yield return null;
    }
}

//Si quisieras parar la ejecución de una corutina, es un "pequeño" problema.
//Tendrias que guardar en una variable qué objeto en concreto la ha iniciado (si sólo se va a llamar aquí no hace falta, pero si por ejemplo
//la empiezas en otro scipt, entonces mejor tener guardado quién ha llamado a esa corutina)
//Además tendrías que guardar en una variable la propia corutina

IEnumerator newCor = MovePlayer();
    StartCoroutine(newCor);
    //StopCoroutine(newCor);
*/


