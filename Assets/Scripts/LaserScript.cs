﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

    public float DesactivateTime;
    public GameObject Laser;

    private float time;
    private bool LaserActivated;

    
	// Use this for initialization
	void Start () {
        time = DesactivateTime;
        LaserActivated = true;
		
	}
	
	// Update is called once per frame
	void Update () {
		//if(Input.GetKeyDown(KeyCode.A))
  //      {
  //          InactivateLaser();            
  //      }
        //despues de activar el interruptor, habra una cuenta atras que al acabar volvera a activar el laser;
        if (LaserActivated == false)
        {
            time -= Time.deltaTime;
            if( time <= 0)
            {
                Laser.SetActive(true);
                LaserActivated = true;
                time = DesactivateTime;
            }
        }
	}

    //Al accionar un interruptor desactivara el laser
    public void InactivateLaser()
    {
        
        Laser.SetActive(false);
        LaserActivated = false;
    }

    public void ResetStart()
    {
        LaserActivated = true;
        time = DesactivateTime;
    }
}