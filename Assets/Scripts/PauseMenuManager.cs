﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour {

    GameManager Manager;
    OptionsManager Options;

    [Header("Effects")]
    GameObject Camera;
    GameObject CronoCanvas;
    GameObject CameraGlitch;
    CameraFilterPack_FX_Glitch1 glitch;
    CameraFilterPack_FX_Glitch1 glitch2;

    public GameObject[] btns;
    public Color SelectedColor;
    public Color CurrentColor;
    public Color blockedColor;

    public bool inMain;

    [Header("Settings Menu")] 
    public GameObject optionsPanel;
    public AudioMixer audioMixer;
    public Toggle fullScrenToggle;
    [SerializeField] TextMeshProUGUI OffText;
    [SerializeField] TextMeshProUGUI OnText;
    public Slider sliderM;
    public Slider sliderE;
    public GameObject fillSliderM;
    public GameObject fillSliderE;


    private void Awake()
    {
        Camera = GameObject.FindGameObjectWithTag("MainCamera");
        CameraGlitch = GameObject.FindGameObjectWithTag("Camera2");
        CronoCanvas = GameObject.FindGameObjectWithTag("CronoCanvas");
        this.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        Options = OptionsManager.Instance;
        Manager = GameManager.Instance;
        inMain = true;
        EventSystem.current.SetSelectedGameObject(btns[0]);
        optionsPanel.SetActive(false);

        //----Settings Menu
        //SetVMusic(Options.volumeM);
        //SetVEffects(Options.volumeE);

        //sliderE.value
        glitch2 = Camera.GetComponent<CameraFilterPack_FX_Glitch1>();
        glitch = CameraGlitch.GetComponent<CameraFilterPack_FX_Glitch1>();
        //this.gameObject.SetActive(false);

        SetVMusic(Options.volumeM);
        SetVEffects(Options.volumeE);
        SetFullScreen(Options.isFullscreen);

        sliderM.value = Options.volumeM;
        sliderE.value = Options.volumeE;
        fullScrenToggle.isOn = Options.isFullscreen;

        StartCoroutine(Startglitch(glitch));
    }

    void Update()
    {
        if (glitch2.enabled == false)
            glitch2.enabled = true;


        for (int i = 0; i < btns.Length; i++){         

            if (btns[i] == EventSystem.current.currentSelectedGameObject){

                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (btns[i].GetComponentInChildren<TextMeshProUGUI>())
                    btns[i].GetComponentInChildren<TextMeshProUGUI>().color = SelectedColor;

                if (btns[i].name == "FullScreenBT")
                {
                    OffText.color = SelectedColor;
                    OnText.color = SelectedColor;
                }

                if (btns[i].GetComponentInChildren<Slider>() )// && (sliderM.value != sliderM.minValue) || (sliderE.value != sliderE.minValue))
                {
                    btns[i].transform.GetChild(2).GetChild(0).GetComponent<Image>().color = SelectedColor;
                }
            }else{

                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (btns[i].GetComponentInChildren<TextMeshProUGUI>())
                    btns[i].GetComponentInChildren<TextMeshProUGUI>().color = CurrentColor;

                if (btns[i].name == "FullScreenBT")
                {
                    OffText.color = CurrentColor;
                    OnText.color = CurrentColor;
                }

                if (btns[i].GetComponentInChildren<Slider>() )//&& (sliderM.value != sliderM.minValue) || (sliderE.value != sliderE.minValue))
                {
                    btns[i].transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().color = CurrentColor;
                }
            }
        }

        if (sliderM.value == sliderM.minValue)
            fillSliderM.GetComponent<Image>().color = Color.clear;
        if (sliderE.value == sliderE.minValue)
            fillSliderE.GetComponent<Image>().color = Color.clear;

        if (CronoCanvas.activeSelf == true){
            CronoCanvas.SetActive(false);
            StartCoroutine(Closed(glitch));
        }
    }

    IEnumerator glitchMenu(MonoBehaviour effect)
    {
        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.1f);
        effect.enabled = !effect.enabled;

    }

    IEnumerator Startglitch(MonoBehaviour effect)
    {
        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.2f);
        effect.enabled = !effect.enabled;

    }

    public void ResumeGame()
    {
        StartCoroutine(ClosedMenuPause(glitch));
    }

    IEnumerator ClosedMenuPause(MonoBehaviour effect)
    {
        glitch.enabled = !glitch.enabled;
        glitch2.enabled = false;
        yield return new WaitForSecondsRealtime(0.1f);
        effect.enabled = !effect.enabled;
        CronoCanvas.SetActive(true);
        this.gameObject.SetActive(false);
        glitch2.enabled = false;
        Manager.SetMenuPause(false);

    }

    //----Settings Menu
    public void SetVMusic(float volume)
    {
        //SetVMusic(sliderM.value);
        Options.SetVMusic(volume);
    }
    public void SetVEffects(float volume)
    {
        //SetVEffects(sliderE.value);
        Options.SetVEffects(volume);
    }

    public void openOptions()
    {
        if(inMain){
            inMain = false;
            optionsPanel.SetActive(true);
            StartCoroutine(Closed(glitch));
            EventSystem.current.SetSelectedGameObject(btns[3]);
        }
    }

    public void closeOptions()
    {
        inMain = true;
        optionsPanel.SetActive(false);
        StartCoroutine(Closed(glitch));
        EventSystem.current.SetSelectedGameObject(btns[1]);
    }

    IEnumerator Closed(MonoBehaviour effect)
    {
        glitch.enabled = !glitch.enabled;
        yield return new WaitForSecondsRealtime(0.1f);
        effect.enabled = !effect.enabled;

    }

    public void SetFullScreen(bool isFullscreen)
    {
        Options.SetFullScreen(isFullscreen);
        OffText.enabled = !isFullscreen;
    }

    public void BackMainMenu()
    {
        Manager.BackMain();
    }

}
