﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CctvController : MonoBehaviour {

    public float rotation;
    public float time_rotation;
    public float time_limit;
    


    private float max_rotation;
    private float min_rotation;
    private float velocity;
    private bool up;
    private bool down;
    private float speed;
    private float storedtimelimit;
    private bool waittime;
    private bool time_start;
    private Quaternion start_angle;
    private float startangle;
    private float timecrono;


    private float angle;
    public bool isShowed;

    GameManager Manager;

    private void Awake()
    {
        isShowed = false;
    }

    // Use this for initialization
    void Start () {
        Manager = GameManager.Instance;
        isShowed = false;
        waittime = false;
        storedtimelimit = time_limit;
        angle = ToAngles(transform.eulerAngles.y);
        startangle = transform.eulerAngles.y;
        start_angle = transform.rotation;
        max_rotation = angle + (rotation / 2);
        min_rotation = angle - (rotation / 2);
        speed = rotation / time_rotation;
        velocity = speed * Time.deltaTime;
        up = false;
        down = true;
        time_start = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (isShowed && Manager.GetShow())
        {
            isShowed = false;
        }

        if (!isShowed && !Manager.GetShow())
        {
            ResetStart();
            isShowed = true;
        }

        if (Manager.GetDetected())
        {
            ResetStart();
        }

        if (Manager.GetShow() || !Manager.GetRec() && !Manager.GetRestart())
            move();

       

    }

    void move()
    {
        if (startangle <= 210 && startangle >= 170)
        {
            angle = transform.eulerAngles.y;
        }
        else
        {
            angle = ToAngles(transform.eulerAngles.y);
        }

        timecrono += Time.deltaTime;

        if (time_start == true)
        {
            Wait();
            if (waittime == false)
            {
                time_start = false;
                storedtimelimit = time_limit;
            }
        }


        if (angle <= min_rotation)
        {
            time_start = true;
            up = false;
            down = true;

        }
        if (angle >= max_rotation)
        {

            time_start = true;
            up = true;
            down = false;

        }



        if (down == true && waittime == false && (Manager.GetShow() || !Manager.GetRec()))
        {
            transform.Rotate(0, velocity, 0);


        }
        if (up == true && waittime == false && (Manager.GetShow() || !Manager.GetRec()))
        {
            transform.Rotate(0, -velocity, 0);

        }

        if (timecrono == 0)
        {
            ResetStart();
        }
    }



    void Wait()
    {
        waittime = true;
        storedtimelimit -= Time.deltaTime;
        if (storedtimelimit <= 0)
        {
            waittime = false;
        }
        
    }

    //conviete angulos engler a angulos normales
    float ToAngles(float angles)
    {
        angles %= 360;
        if (angles > 180)
        {
            return angles - 360;
        }
        return angles;
    }

    public void ResetStart()
    {
        transform.rotation = start_angle;
        waittime = false;
        time_start = false;
        up = false;
        down = true;
        storedtimelimit = time_limit;
        timecrono = 0;
    }
}
