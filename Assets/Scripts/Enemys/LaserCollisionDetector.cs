﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCollisionDetector : MonoBehaviour {

    private BoxCollider colliderLaser;

	// Use this for initialization
	void Start () {
        colliderLaser = GetComponent<BoxCollider>();
        colliderLaser.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.Instance.GetRec() == false)
        {
            colliderLaser.isTrigger = false;
        }
        else
        {
            colliderLaser.isTrigger = true;
        }
	}
    void OnCollisionEnter(Collision coll)
    {

        //Detectara si el jugador ha collisionado con el laser
        
            if (coll.collider.tag == "Player")
            {
                PlayerDetectado();
            }
        
    }

    //Esta funcion se activara cuando el personaje haga collision con el laser
    public void PlayerDetectado()
    {
        GameManager.Instance.SetDetected(true);
    }
}
