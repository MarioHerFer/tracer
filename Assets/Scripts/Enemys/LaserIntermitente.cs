﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserIntermitente : MonoBehaviour {

    public float timeActivated;
    public GameObject Laser;
    //public GameObject Cronometro;
    
    //private Cronotime Cronoscript;
    private float timetotal;
    private float timeseconds;
    private bool activatedOnce;
    private bool isShowed;

    GameManager Manager;

    // Use this for initialization
    void Start ()
    {
        Manager = GameManager.Instance;
        isShowed = false;
        activatedOnce = false;
        //Cronoscript = Cronometro.GetComponent<Cronotime>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isShowed && Manager.GetShow())
        {
            isShowed = false;
        }

        if (!isShowed && !Manager.GetShow()){
            isShowed = true;
            ResetStart();
        }
        if (Manager.GetShow() || !Manager.GetRec())
            pop();

        if (Manager.GetDetected())
        {
            ResetStart();
        }
    }


    void pop()
    {
        timetotal +=Time.deltaTime;
        //timeseconds = Cronoscript.getSeconds();
        if (((int)timetotal % timeActivated) == 0)
        {
            //if (Laser.active == false && activatedOnce == false )
            if (Laser.activeSelf == false && activatedOnce == false){
                Laser.SetActive(true);
                activatedOnce = true;

            }else if (Laser.activeSelf == true && activatedOnce == false){
                Laser.SetActive(false);
                activatedOnce = true;
            }
        }else{
            activatedOnce = false;
        }
    }

    public void ResetStart()
    {
        Laser.SetActive(true);
    }
    public void OnOff()
    {

    }
}

/*
timetotal = Cronoscript.getRecTime();
timeseconds = Cronoscript.getSeconds();

if(GameManager.Instance.GetRec() == false || GameManager.Instance.GetShow())
{
    if (timeseconds % timeActivated == 0 && timetotal > 0)
    {

        //if (Laser.active == false && activatedOnce == false )
        if (Laser.activeSelf == false && activatedOnce == false )
        {
            Laser.SetActive(true);
            activatedOnce = true;

        }
        else if(Laser.activeSelf == true && activatedOnce == false)
        {
            Laser.SetActive(false);
            activatedOnce = true;

        }

    }
    else
    {
        activatedOnce = false;
    }

}
if (timetotal == 0)
{
    ResetStart();
}*/
