﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interruptor : MonoBehaviour {

    public GameObject laserObject;

    private LaserScript laserIntScript;

	// Use this for initialization
	void Start () {
        laserIntScript = laserObject.GetComponent<LaserScript>();

	}
	
	// Update is called once per frame
	



    //Al ser activado desactivara el laser;
    void OnTriggerEnter(Collider coll)
    {
        
        if (GameManager.Instance.GetRec() == false)
        { 
            if (coll.tag == "Player")
            {
 
                laserIntScript.InactivateLaser();
            }
        }
    }
}
