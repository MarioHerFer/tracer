﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaserController : MonoBehaviour {


    public float rotation;
    public float time_rotation;
    public float time_limit;
    public float laserlenght;
    public enum Frente { delante, detras, arriba, abajo, izquierda, derecha };
    public Frente frente;
    public GameObject LaserRebots;
    public GameObject LaserRebots2;

    private Vector3 DirectCamera;
    private RaycastHit hit;
    private LineRenderer LaserRebote;
    private LineRenderer LaserRebote2;
    private float max_rotation;
    private float min_rotation;
    private float velocity;
    private bool up;
    private bool down;
    private float speed;
    private float storedtimelimit;
    private bool waittime;
    private bool time_start;
    private float angle;
    private LineRenderer Laser;
    private float timecrono;
    private bool isShowed;

    GameManager Manager;
    private float startangle;
    private Quaternion start_angle;

    // Use this for initialization
    void Start () {
        Manager = GameManager.Instance;
        isShowed = false;
        waittime = false;
        storedtimelimit = time_limit;
        angle = ToAngles(transform.localEulerAngles.y);

        startangle = transform.eulerAngles.y;

        start_angle = transform.rotation;

        max_rotation = angle + (rotation / 2);
        min_rotation = angle - (rotation / 2);

        Laser = GetComponent<LineRenderer>();

        speed = rotation / time_rotation;
        velocity = speed * Time.deltaTime;

        up = false;
        down = true;
        time_start = false;

        LaserRebote = LaserRebots.GetComponent<LineRenderer>();
        LaserRebote2 = LaserRebots2.GetComponent<LineRenderer>();
        //Donde saldra el laser
        /*switch (frente)
        {
            case Frente.delante:
                DirectCamera = Vector3.forward;
                break;
            case Frente.detras:
                DirectCamera = Vector3.back;
                break;
            case Frente.arriba:
                DirectCamera = Vector3.up;
                break;
            case Frente.abajo:
                DirectCamera = Vector3.down;
                break;
            case Frente.izquierda:
                DirectCamera = Vector3.left;
                break;
            case Frente.derecha:
                DirectCamera = Vector3.right;
                break;
            default:
                DirectCamera = Vector3.left;
                break;

        }*/
        DirectCamera = Vector3.forward;
    }

    void Update()
    {
        if (isShowed && Manager.GetShow())
        {
            isShowed = false;
        }

        if (!isShowed && !Manager.GetShow())
        {
            isShowed = true;
            ResetStart();
        }

        if (Manager.GetShow() || !Manager.GetRec() && !Manager.GetRestart())
        {
            Laser.enabled = true;
            LaserRebote.enabled = true;
            LaserRebote2.enabled = true;
            pop();
            pop2();
        }

        if (Manager.GetDetected())
        {
            ResetStart();
        }

    }
	
    void pop()
    {
        if (startangle <= 210 && startangle >= 170)
        {
            angle = transform.eulerAngles.y;
        }
        else
        {
            angle = ToAngles(transform.eulerAngles.y);
        }

        timecrono += Time.deltaTime;

        if (time_start == true)
        {
            Wait();
            if (waittime == false)
            {
                time_start = false;
                storedtimelimit = time_limit;
            }
        }


        if (angle <= min_rotation)
        {
            time_start = true;
            up = false;
            down = true;

        }
        if (angle >= max_rotation)
        {

            time_start = true;
            up = true;
            down = false;

        }



        if (down == true && waittime == false && (Manager.GetShow() || !Manager.GetRec()))
        {
            transform.Rotate(0, velocity, 0);


        }
        if (up == true && waittime == false && (Manager.GetShow() || !Manager.GetRec()))
        {
            transform.Rotate(0, -velocity, 0);

        }

        if (timecrono == 0)
        {
            ResetStart();
        }
    }
    // Update is called once per frame
    void pop2(){
        
            //Laser, este laser matara al jugador, si choca contra una pared rebotara y si no seguira
            bool coll = Physics.Raycast(transform.position, transform.TransformDirection(DirectCamera), out hit, laserlenght);
            if (coll == false)
            {
                
                //aqui no choca con nadie
                Laser.SetPosition(0, transform.position);
                Laser.SetPosition(1, transform.position + laserlenght * transform.TransformDirection(DirectCamera));
                Laser.startColor = Color.green;
                Laser.endColor = Color.green;
                LaserRebote.enabled = false;
                LaserRebote2.enabled = false;
                
                //Debug.DrawRay(transform.position, transform.TransformDirection(DirectCamera) * 1000, Color.white, timeLasers);
            }
            else
            {
                if (hit.collider.tag == "Pared")
                {
                    //choca con la pared y rebota
                    Laser.SetPosition(0, transform.position);
                    Laser.SetPosition(1, transform.position + hit.distance * transform.TransformDirection(DirectCamera));
                    Laser.startColor = Color.green;
                    Laser.endColor = Color.green;
                    //Debug.DrawRay(transform.position, transform.TransformDirection(DirectCamera) * hit.distance, Color.white, timeLasers);
                    
                    RaycastHit hitbounce;
                    Vector3 position = hit.point;
                    Vector3 direction = Vector3.Reflect(transform.TransformDirection(DirectCamera), hit.normal);
                
                    bool collis = Physics.Raycast(position, direction, out hitbounce, laserlenght);
                    if (collis == true)
                    {

                        if (hitbounce.collider.tag == "Player" && GameManager.Instance.GetRec() == false)
                        {
                            //el rebote choca con el jugador


                            LaserRebote.enabled = true;
                            LaserRebote.SetPosition(0, position);
                            LaserRebote.SetPosition(1, position + hitbounce.distance * direction);
                            LaserRebote.startColor = Color.red;
                            LaserRebote.endColor = Color.red;
                            LaserRebote2.enabled = false;
                            PlayerDetectado();
                            //Debug.DrawRay(position, direction * hitbounce.distance, Color.red, timeLasers);


                        }
                        else
                        {
                            //el rebote choca contra la pared y rebota
                            LaserRebote.enabled = true;
                            LaserRebote.SetPosition(0, position);
                            LaserRebote.SetPosition(1, position + hitbounce.distance * direction);
                            LaserRebote.startColor = Color.green;
                            LaserRebote.endColor = Color.green;

                            RaycastHit hitbounce2 = new RaycastHit();
                            Vector3 positioncoll2 = hitbounce.point;
                            Vector3 directioncoll2 = Vector3.Reflect(transform.TransformDirection(DirectCamera), hitbounce2.normal);

                            bool RayCollis = Physics.Raycast(positioncoll2, directioncoll2, out hitbounce2, laserlenght);
                            if (RayCollis == true)
                            {
                                
                                if (hitbounce2.collider.tag == "Player" && GameManager.Instance.GetRec() == false)
                                {
                                    //el segundo rebote del laser choca contra el jugador
                                    LaserRebote2.enabled = true;
                                    LaserRebote2.SetPosition(0, positioncoll2);
                                    LaserRebote2.SetPosition(1, positioncoll2 + hitbounce2.distance * directioncoll2);
                                    LaserRebote2.startColor = Color.red;
                                    LaserRebote2.endColor = Color.red;

                                    PlayerDetectado();

                                }
                                else
                                {
                                    //el segundo rebote del laser choca contra la pared
                                    LaserRebote2.enabled = true;
                                    LaserRebote2.SetPosition(0, positioncoll2);
                                    LaserRebote2.SetPosition(1, positioncoll2 + hitbounce2.distance * directioncoll2);
                                    LaserRebote2.startColor = Color.green;
                                    LaserRebote2.endColor = Color.green;

                                }
                            }
                            else
                            {
                                //el segundo rebote del laser no choca
                                LaserRebote2.enabled = true;
                                LaserRebote2.SetPosition(0, positioncoll2);
                                LaserRebote2.SetPosition(1, positioncoll2 + laserlenght * directioncoll2);
                                LaserRebote2.startColor = Color.green;
                                LaserRebote2.endColor = Color.green;

                            }
                        }


                    }
                    else
                    {
                        //el rebote del laser no choca
                        LaserRebote.SetPosition(0, position);
                        LaserRebote.SetPosition(1, position + laserlenght * direction);
                        LaserRebote.startColor = Color.green;
                        LaserRebote.endColor = Color.green;
                        LaserRebote2.enabled = false;

                    }
                }
                if (hit.collider.tag == "Player" && GameManager.Instance.GetRec() == false)
                {
                    //el laser choca contra el jugador
                    Laser.SetPosition(0, transform.position);
                    Laser.SetPosition(1, transform.position + hit.distance * transform.TransformDirection(DirectCamera));
                    Laser.startColor = Color.red;
                    Laser.endColor = Color.red;
                    LaserRebote.enabled = false;
                    LaserRebote2.enabled = false;

                    PlayerDetectado();

                }
                if (timecrono == 0)
                {
                    ResetStart();
                }
            }
        

    }
    //el enemigo esperara durante unos segundo en sus limites
    public void Wait()
    {
        
        waittime = true;
        storedtimelimit -= Time.deltaTime;
        if (storedtimelimit <= 0)
        {
            waittime = false;
        }
        
    }

    //conviete angulos engler a angulos normales
    public float ToAngles(float angles)
    {
        angles %= 360;
        if (angles > 180)
        {
            return angles - 360;
        }
        return angles;
    }
    //El enemigo al detectar el jugador usara esta funcion
    private void PlayerDetectado()
    {
        GameManager.Instance.SetDetected(true);
    }

    public void ResetStart()
    {
        transform.rotation = start_angle;
        waittime = false;
        time_start = false;
        up = false;
        down = true;
        storedtimelimit = time_limit;
        Laser.enabled = false;
        LaserRebote.enabled = false;
        LaserRebote2.enabled = false;
    }
}
