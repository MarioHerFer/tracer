﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class effectManager : MonoBehaviour {

    CameraFilterPack_FX_Glitch1 glitch;
    CameraFilterPack_TV_VHS_Rewind Rewind;
    CameraFilterPack_VHS_Tracking Rewind2;
    [SerializeField] ControlPlayer Player;


	// Use this for initialization
	void Start () {

        // glitch1 = wieuf.GetComponent<GlitchEffect>();
        glitch = GetComponent<CameraFilterPack_FX_Glitch1>();
        Rewind = GetComponent<CameraFilterPack_TV_VHS_Rewind>();
        Rewind2 = GetComponent<CameraFilterPack_VHS_Tracking>();

        StartCoroutine(Startglitch(glitch));
    }

    void Update()
    {
        if (Player.Back || Player.ReBack){
            Rewind.enabled = true;
            Rewind2.enabled = true;
        }else
        {
            Rewind.enabled = false;
            Rewind2.enabled = false;
        }

    }

    public void theGlitch()
    {
        StartCoroutine(Startglitch(glitch));
    }

    IEnumerator Startglitch(MonoBehaviour effect)
    {
        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.2f);
        effect.enabled = !effect.enabled;

    }

}
