﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    GameManager Manager;
    OptionsManager Options;

    [Header("Effects")]
    [SerializeField] GameObject Camera;
    [SerializeField] GameObject Camera2;
    CameraFilterPack_FX_Glitch1 glitch;
    CameraFilterPack_FX_Glitch1 glitch2;
    GlitchEffect glitchcolor;

    public GameObject[] btns;
    public Color SelectedColor;
    public Color CurrentColor;
    public Color blockedColor;

    public bool inMain;

    [Header("Levels")]
    public GameObject levelsPanel;
    public GameObject[] LVLS;

    [Header("Settings Menu")] 
    public GameObject optionsPanel;
    public AudioMixer audioMixer;
    public Toggle fullScrenToggle;
    [SerializeField] GameObject Off;
    [SerializeField] GameObject ON;
    private TextMeshProUGUI OffText;
    private TextMeshProUGUI OnText;
    public Slider sliderM;
    public Slider sliderE;
    public GameObject fillSliderM;
    public GameObject fillSliderE;

    private float timeBack; 


    // Use this for initialization
    void Start()
    {
        Options = OptionsManager.Instance;
        Manager = GameManager.Instance;

        inMain = true;
        EventSystem.current.SetSelectedGameObject(btns[0]);

        optionsPanel.SetActive(false);
        levelsPanel.SetActive(false);

        //----Settings Menu
        //SetVMusic(Options.volumeM);
        //SetVEffects(Options.volumeE);

        glitch2 = Camera2.GetComponent<CameraFilterPack_FX_Glitch1>();

        glitch = Camera.GetComponent<CameraFilterPack_FX_Glitch1>();
        glitchcolor = Camera.GetComponent<GlitchEffect>();

        OffText = Off.GetComponent<TextMeshProUGUI>();
        OnText = ON.GetComponent<TextMeshProUGUI>();

        StartCoroutine(BackgroundEffect(glitch, glitchcolor));


        SetVMusic(Options.volumeM);
        SetVEffects(Options.volumeE);
        SetFullScreen(Options.isFullscreen);

        sliderM.value = Options.volumeM;
        sliderE.value = Options.volumeE;

        fullScrenToggle.isOn = Options.isFullscreen;

        
    }


    void Update()
    {
        
        for (int i = 0; i < btns.Length; i++){

            if (btns[i] == EventSystem.current.currentSelectedGameObject){

                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (btns[i].GetComponentInChildren<TextMeshProUGUI>())
                    btns[i].GetComponentInChildren<TextMeshProUGUI>().color = SelectedColor;

                if (btns[i].name == "FullScreenBT"){
                    OffText.color = SelectedColor;
                    OnText.color = SelectedColor;
                }
                //btns[i].transform.GetChild(1).gameObject.GetComponentInChildren<TextMeshProUGUI>().color = SelectedColor;


                if (btns[i].GetComponentInChildren<Slider>() )// && (sliderM.value != sliderM.minValue) || (sliderE.value != sliderE.minValue))
                {
                    btns[i].transform.GetChild(2).GetChild(0).GetComponent<Image>().color = SelectedColor;
                }
            }
            else{

                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (btns[i].GetComponentInChildren<TextMeshProUGUI>())
                    btns[i].GetComponentInChildren<TextMeshProUGUI>().color = CurrentColor;

                if (btns[i].name == "FullScreenBT"){
                    OffText.color = CurrentColor;
                    OnText.color = CurrentColor;
                }


                if (btns[i].GetComponentInChildren<Slider>() )//&& (sliderM.value != sliderM.minValue) || (sliderE.value != sliderE.minValue))
                {
                    btns[i].transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().color = CurrentColor;
                }
            }
        }

        if (sliderM.value == sliderM.minValue)
            fillSliderM.GetComponent<Image>().color = Color.clear;
        if (sliderE.value == sliderE.minValue)
            fillSliderE.GetComponent<Image>().color = Color.clear;


        for (int i = 0; i < LVLS.Length; i++)
        {
            if (LVLS[i] == EventSystem.current.currentSelectedGameObject)
            {
                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (LVLS[i].GetComponentInChildren<TextMeshProUGUI>())
                    LVLS[i].GetComponentInChildren<TextMeshProUGUI>().color = SelectedColor;                
            }else{
                
                if (!inMain)
                    for (int j = 0; j < 3; j++)
                        btns[j].GetComponentInChildren<TextMeshProUGUI>().color = blockedColor;

                if (LVLS[i].GetComponentInChildren<TextMeshProUGUI>())
                    LVLS[i].GetComponentInChildren<TextMeshProUGUI>().color = CurrentColor;

            }
        }

    }
    

    //public void Credits()
    //{
    //    GameObject.Find("MadeBy").GetComponent<TMPro.TextMeshProUGUI>().enabled = true;
    //}

    public void ExitGame()
    {
        Application.Quit();
    }

    //----Settings Menu

    public void SetVMusic(float volume)
    {
        //SetVMusic(sliderM.value);
        Options.SetVMusic(volume);
    }
    public void SetVEffects(float volume)
    {
        //SetVEffects(sliderE.value);
        Options.SetVEffects(volume);
    }


    public void openOptions()
    {
        if(inMain){
            inMain = false;
            optionsPanel.SetActive(true);
            StartCoroutine(Closed(glitch2));
            EventSystem.current.SetSelectedGameObject(btns[3]);
        }
    }

    public void closeOptions()
    {
        inMain = true;
        optionsPanel.SetActive(false);
        StartCoroutine(Closed(glitch2));
        EventSystem.current.SetSelectedGameObject(btns[1]);
    }

    public void openLevels()
    {
        if (inMain){
            inMain = false;
            levelsPanel.SetActive(true);
            StartCoroutine(Closed(glitch2));
            EventSystem.current.SetSelectedGameObject(LVLS[0]);
        }
    }

    public void closelevels()
    {
        inMain = true;
        levelsPanel.SetActive(false);
        StartCoroutine(Closed(glitch2));
        EventSystem.current.SetSelectedGameObject(btns[0]);
    }

    IEnumerator Closed(MonoBehaviour effect)
    {

        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.1f);
        effect.enabled = !effect.enabled;

    }

    IEnumerator BackgroundEffect(MonoBehaviour effect, MonoBehaviour effect2)
    {
        while (true){
            /*effect.enabled = !effect.enabled;
            effect2.enabled = !effect2.enabled;*/
            yield return new WaitForSecondsRealtime(timeBack);
            effect.enabled = !effect.enabled;
            effect2.enabled = !effect2.enabled;
            timeBack = Random.Range(1f, 3.5f);
        }
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Options.SetFullScreen(isFullscreen);
        OffText.enabled = !isFullscreen;
    }

    public void ChangeS(int scene)
    {
        StartCoroutine(nextLevel(glitch2,scene));
    }

    IEnumerator nextLevel(MonoBehaviour effect, int scene)
    {
        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.2f);
        effect.enabled = !effect.enabled;
        Manager.LoadScene(scene);
    }
}
