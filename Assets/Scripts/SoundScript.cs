﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour {

    // Como yo lo hago no hace falta tener aqui el player
    // Lo que pides del player lo puedes conseguir del GameManager
    // Te dije que te mirarses las funciones del GameManager
    // Manager.GetBACK() te devuele si esta rebobinando o no
    // Lo he utilizado para más abajo


    //[SerializeField]ControlPlayer player;

    public AudioSource[]  Audio;
    //private bool Played;
    //---------------------------------------------
    GameManager Manager;
    private bool startRewind;
    private bool currentRewind;

	void Start () {
        Audio = GetComponents<AudioSource>();
        //Played = false;
        Audio[0].Play();
        //-------------------------------
        
        Manager = GameManager.Instance;
        //estos booleanos son para indicar
        //si se ha entrado en la fase1 y en la fase2, respectivamente
        startRewind = false;
        currentRewind = false;
    }
	
	void Update () {
        /*
        if (Input.GetKeyDown(KeyCode.F) && !GameManager.Instance.GetShow())
        {
            Audio[0].volume = 0;
            Audio[1].Play();
            
        }

        if (Audio[1].isPlaying == false && (player.Back || player.ReBack) && Played == false )
        {
            Audio[0].volume = 0;
            Audio[1].Stop();
            Audio[2].Play();
            Played = true;
            
        }

        if(Input.GetKeyUp(KeyCode.F) && !GameManager.Instance.GetShow())
        {
            Audio[2].Stop();
            Audio[3].Play();
        }
        

        if (!player.Back && !player.ReBack)
        {
            Audio[2].Stop();
            Audio[0].volume = 1;
            Played = false;

        }*/
        //---------------------------------------------
        
        //fase 3
        if (startRewind && !Manager.GetBACK()){
            Audio[2].Stop();
            Audio[3].Play();
            Audio[0].volume = 0.35f;
            startRewind = false;
            currentRewind = false;
        }
        //fase 2
        if (startRewind && Manager.GetBACK() && !Audio[1].isPlaying && !currentRewind){
            currentRewind = true;
            startCurrentRewind();
        }
        //fase 1
        if (!startRewind && Manager.GetBACK()){ 
            Audio[0].volume = 0;
            Audio[1].Play();
            startRewind = true;
        }
        
	}

    //Solo se llama una vez en la fase 2
    private void startCurrentRewind(){
        Audio[2].Play();
    }



}
