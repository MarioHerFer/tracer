﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Cronotime : MonoBehaviour{

    private Text Crono;// , Information;
    private float RecTime;
    public bool paused = true;
    private int initialTime,miliseconds, seconds, minutes;

    [SerializeField] Image  Play, Rec, Pause, Remove;

    public bool Back;

    private Color on, onRec, off, offRec;

    GameManager Manager;

    void Start () {
        on = new Color(1, 1, 1, 1);
        off = new Color(1, 1, 1, 0.5f);

        onRec = new Color(1, 0, 0, 1);
        offRec = new Color(1, 0, 0, 0.5f);

        Crono = GameObject.Find("Crono").GetComponent<Text>();
        //Information = GameObject.Find("Information").GetComponent<Text>();
        Manager = GameManager.Instance;

        Manager.crono = this;
    }


    void Update () {

        Back = Manager.GetBACK();

        if (paused){
            Pause.GetComponent<Image>().color = on;
            Play.GetComponent<Image>().color = off;
            Rec.GetComponent<Image>().color = offRec;
        }else{
            RecTime += Time.fixedDeltaTime;
            Pause.GetComponent<Image>().color = off;
            Rec.GetComponent<Image>().color = Color.Lerp(onRec, offRec, Mathf.PingPong(Time.time, 0.8f));
        }

        if(!Manager.GetRec()){
            Rec.GetComponent<Image>().color = offRec;
            Play.GetComponent<Image>().color = on;
        }
        
        if (Back){
            Remove.GetComponent<Image>().color = on;
            Play.GetComponent<Image>().color = off;
            Pause.GetComponent<Image>().color = off;
            RecTime -= Time.fixedDeltaTime;
            if (RecTime <= 0)
                RecTime = 0;
            if (Manager.GetDetected())
                Pause.GetComponent<Image>().color = off;
        }
        else{
            Remove.GetComponent<Image>().color = off;
        }

        RefreshWatch(RecTime);
        
    }

    public void RefreshWatch(float timeseconds)
    {
        seconds = (int)timeseconds % 60;
        miliseconds = (int)((timeseconds - seconds) * 100);
        minutes = (int)timeseconds / 60;

        Crono.text = (minutes.ToString("00") + ":" + seconds.ToString("00") + "." + miliseconds.ToString("00")).ToString();

    }

    public void ResetCrono(){
        RecTime = 0f;
    }

    public float getRecTime(){
        return RecTime;
    }

    public float getSeconds()
    {
        return seconds;
    }

}
