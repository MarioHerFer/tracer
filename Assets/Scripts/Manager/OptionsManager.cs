﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OptionsManager : Singleton<OptionsManager> {

    public AudioMixer audioMixer;

    public float volumeM = 7;
    public float volumeE = 7;

    public bool isFullscreen = true;

    public void Init_OptionsManager() {

    }
    
    //------Sounds------

    public void SetAudioMixer(AudioMixer _audioMixer)
    {
        audioMixer = _audioMixer;
    }

    public AudioMixer GetAudioMixer()
    {
        return audioMixer;
    }

    public void SetVMusic(float volume)
    {
        volume = ajustVolume(volume);

        audioMixer.SetFloat("Music", volume);
        volumeM = volume; 
    }
    public void SetVEffects(float volume)
    {
        volume = ajustVolume(volume);

        audioMixer.SetFloat("Effects", volume);
        volumeE = volume;
    }

    float ajustVolume(float volume)
    {
        if (volume > 0)
        {
            //volume = (volume - 80) + 65;
        }
        else
            volume = -80;

        return volume;
    }

    public void SetFullScreen(bool _isFullscreen)
    {
        Screen.fullScreen = _isFullscreen;
        isFullscreen = _isFullscreen;
    }
}
