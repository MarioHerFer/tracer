﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public Cronotime crono;

    public ControlPlayer controlPlayer;

    private GameObject TheLight;

    public GameObject ThePauseMenu;
    public GameObject ShowPanel;

    public GameObject mainCamera;
    public GameObject Camera2;

    public ShowLevel ShowManager;

    private bool BACK = false;

    private bool SHOW = false;

    private bool DETECTED = false;

    private bool MenuPause = false;

    public void Init_GameManager()
    {
            FindLevelObjects();
    }
    
    private void FindLevelObjects(){
        
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            TheLight = GameObject.Find("Light").gameObject;
            //---------------------
            SHOW = GameObject.Find("Manager").GetComponent<ShowLevel>().SHOW;

            ThePauseMenu = GameObject.Find("CanvasPause").transform.GetChild(0).gameObject;
            ShowPanel = GameObject.Find("CanvasPause").transform.GetChild(1).gameObject;

            mainCamera = GameObject.Find("Main Camera");
            Camera2 = GameObject.Find("Main Camera (2)");
            ShowManager = GameObject.Find("Manager").GetComponent<ShowLevel>();

            ShowManager.Invoke("Init",0f);
            SetMenuPause(false);

        }

    }

    /*************************************************************************************************/
    /**
     * @brief Volver a la escena del MainMenu.
     */
    public void BackMain()
    {
        SceneManager.LoadScene(0);
        Invoke("FindLevelObjects",0.2f);
    }

    /*************************************************************************************************/
    /**
     * @brief Cargar la escena del indice segun el valor recibido.
     * 
     * @param int scene Numero de la escena que se quiere cargar.
     */
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
        Invoke("FindLevelObjects",0.2f);
    }

    /*************************************************************************************************/
    /**
     * @brief Carga la siguiente escena.
     */
    public void NextScene()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 10)
        {
            BackMain();
            return;
        }

        SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
        Invoke("FindLevelObjects",0.2f);
    }

    /*************************************************************************************************/
    /**
     * @brief Reinicia la escena.
     */
    public void ResetScene()
    {
        SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        Invoke("FindLevelObjects", 0.2f);
    }

    /*************************************************************************************************/
    /**
     * @brief Dice si el player esta en el modo de Trazar o no.
     * 
     * @returns bool si player esta en trazado o no.
     */
    public bool GetRec(){
        return controlPlayer.rec;
    }

    /*************************************************************************************************/
    /**
     * @brief Cambia el estado de Rec.
     */
    public void SetRec(bool _Rec){
        controlPlayer.rec = _Rec;
    }

    /*************************************************************************************************/
    /**
     * @brief Permite cambiar el estado del cronometro.
     */
    public void SetPaused(bool _paused){
        crono.paused = _paused;
    }

    /*************************************************************************************************/
    /**
     * @brief Reiniciara el cronometro.
     */
    public void ResetCrono(){
        crono.ResetCrono();
    }

    /*************************************************************************************************/
    /**
     * @brief Permite encender o apagar las luces.
     * 
     * @param bool _Active cambiamos el estado de las Luces de la escena al valor de _Active.
     */
    public void SetLight(bool _Active){
        TheLight.SetActive(_Active);
    }
 
    /*************************************************************************************************/
    /**
     * @brief Dice si el player esta reseteando a la posicion inicial.
     * 
     * @returns bool si player esta reseteando o no.
     */
    public bool GetRestart(){
        return controlPlayer.Restart;
    }

    /*************************************************************************************************/
    /**
     * @brief Devuelve el tiempo que tiene el cronometro.
     * 
     * @returns float Tiempo en el cronometro.
     */
    public float GetRecTime(){
        return crono.getRecTime();
    }

    /*************************************************************************************************/
    /**
     * @brief Dice si el player esta retrocedientdo en el trazado o no.
     * 
     * @returns bool si player esta en trazado o no.
     */
    public bool GetBACK(){
        return BACK;
    }

    /*************************************************************************************************/
    /**
     * @brief Permite cambiar el estado de la fase BACK.
     * 
     * @param bool _BACK cambiamos el estado de la fase BACK al valor de _BACK.
     */
    public void SetBACK(bool _BACK){
        BACK = _BACK;
    }
    
    /*************************************************************************************************/
    /**
     * @brief Dice si se esta mostrando el movimiento de enemigos / Fase SHOW.
     * 
     * @returns bool Si esta en la fase SHOW o no.
     */
    public bool GetShow(){
        return SHOW;
    }

    /*************************************************************************************************/
    /**
     * @brief Permite cambiar el estado de la fase SHOW.
     *
     * @param bool _SHOW cambiamos el estado de la fase SHOW al valor de _SHOW.
     */
    public void SetShow(bool _SHOW){
        SHOW = _SHOW;
    }

    /*************************************************************************************************/
    /**
     * @brief Quita el canvas de la fase SHOW.
     */
    public void StopShow()
    {
        ShowPanel.GetComponent<ShowPanelManager>().ClosedPanel();
        ShowManager.setStopShow(true);
    }
    
    /*************************************************************************************************/
    /**
     * @brief Dice si se esta mostrando el menu de pausa o no.
     * 
     * @returns bool Si esta activado el menu de pausa.
     */
    public bool GetMenuPause(){
        return MenuPause;
    }

    /*************************************************************************************************/
    /**
     * @brief Permite cambiar el estado del menu de pausa.
     *
     * @param bool _MenuPause cambiamos el estado del menu de pausa al valor de _MenuPause.
     */
    public void SetMenuPause(bool _MenuPause){
        MenuPause = _MenuPause;
        ThePauseMenu.SetActive(_MenuPause);
        ThePauseMenu.SetActive(_MenuPause);
    }
    
    /*************************************************************************************************/
    /**
     * @brief Dice si el jugador ha sido detectado o no.
     * 
     * @returns bool Si has sido detectado o no.
     */
    public bool GetDetected()
    {
        return DETECTED;
    }
    
    /*************************************************************************************************/
    /**
     * @brief Permite cambiar el estado de la fase DETECTED.
     *
     * @param bool _DETECTED cambiamos el estado  de la fase DETECTED al valor de _DETECTED.
     */
    public void SetDetected(bool _DETECTED)
    {
        DETECTED = _DETECTED;
        if (DETECTED)
        {
            controlPlayer.ResetStart();
            SetLight(false);
        }

    }

}
