﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class InitSingleton : MonoBehaviour {

    public AudioMixer audioMixer;

    //GG
    void Awake()
    {
        if (GameObject.Find("(singleton) GameManager") != null) return;

        GameManager.Instance.Init_GameManager();
        OptionsManager.Instance.Init_OptionsManager();
        OptionsManager.Instance.SetAudioMixer(audioMixer);
    }
}
