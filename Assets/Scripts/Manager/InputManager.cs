﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
    
    private ControlPlayer ControlPlayer;
    //private ShowLevel Show;

    GameManager Manager;

    // Use this for initialization
    void Start () {
        
        if (GameObject.Find("Player"))
            ControlPlayer = GameObject.Find("Player").GetComponent<ControlPlayer>();
        else
            ControlPlayer = null;

        //Show = GetComponent<ShowLevel>();

        Manager = GameManager.Instance;

    }
	
	// Update is called once per frame
	void Update () {

        if (ControlPlayer && !Manager.GetMenuPause() && !Manager.GetShow())
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (Manager.GetRec())
                    ControlPlayer.ResetStart();
            }
            //if (Input.GetKeyDown(KeyCode.Space)) ControlPlayer.StartTracer();

            if (Manager.GetRecTime() >= 0){
                if (Input.GetKey(KeyCode.F)) ControlPlayer.Remove();
                if (Input.GetKeyUp(KeyCode.F)) Manager.SetBACK(false);
            }
            //if (Input.GetKeyDown(KeyCode.L)) Show.SHOW = true;

            if (ControlPlayer.rec){
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Invoke("openMenu",0.3f);
                }
            }
        }

        if (Manager.GetShow())
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Manager.StopShow();

            }
        }
    }

    void openMenu()
    {
        Manager.SetMenuPause(true);
    }
}
