﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class effectManagerMain : MonoBehaviour {

    CameraFilterPack_FX_Glitch1 glitch;


	// Use this for initialization
	void Start () {

        // glitch1 = wieuf.GetComponent<GlitchEffect>();
        glitch = GetComponent<CameraFilterPack_FX_Glitch1>();

        StartCoroutine(Startglitch(glitch));
    }
    
    IEnumerator Startglitch(MonoBehaviour effect)
    {
        effect.enabled = !effect.enabled;
        yield return new WaitForSecondsRealtime(0.2f);
        effect.enabled = !effect.enabled;

    }

}
