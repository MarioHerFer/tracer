﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CctvController1 : MonoBehaviour {

    public float rotation;
    public float time_rotation;
    public float time_limit;


    private float max_rotation;
    private float min_rotation;
    private float velocity;
    private bool up;
    private bool down;
    private float speed;
    private float storedtimelimit;
    private bool waittime;
    private bool time_start;
    private Quaternion max_quat;
    private Quaternion min_quat;

    // Use this for initialization
    void Start()
    {


        waittime = false;
        storedtimelimit = time_limit;
        max_rotation = 0 + (rotation / 2);
        min_rotation = 0 - (rotation / 2);
        speed = rotation / time_rotation;
        velocity = speed * Time.deltaTime;
        up = false;
        down = true;
        time_start = false;
        max_quat.Set(0, 0, max_rotation, 0);
        min_quat.Set(0, 0, min_rotation, 0);

    }

    // Update is called once per frame
    void Update()
    {
        if (time_start == true)
        {
            Wait();
            if (waittime == false)
            {
                time_start = false;
                storedtimelimit = time_limit;
            }
        }


        if (((transform.rotation.z * 100) <= min_rotation))
        {
            time_start = true;
            up = false;
            down = true;




        }
        if ((transform.rotation.z * 100) >= max_rotation)
        {

            time_start = true;
            up = true;
            down = false;



        }



        if (down == true && waittime == false)
        {
            transform.Rotate(0, 0, velocity);

        }
        if (up == true && waittime == false)
        {
            transform.Rotate(0, 0, -velocity);
        }


    }
    public void Wait()
    {


        waittime = true;
        storedtimelimit -= Time.deltaTime;
        if (storedtimelimit <= 0)
        {
            waittime = false;
        }



    }
}
