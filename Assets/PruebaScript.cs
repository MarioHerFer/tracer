﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.UpArrow) == true)
        {
            transform.position += transform.forward;
        }
        if (Input.GetKey(KeyCode.DownArrow) == true)
        {
            transform.position -= transform.forward;
        }
        if (Input.GetKey(KeyCode.W) == true)
        {
            transform.position += transform.up;
        }
        if (Input.GetKey(KeyCode.S) == true)
        {
            transform.position -= transform.up;
        }
        if (Input.GetKey(KeyCode.A) == true)
        {
            transform.position -= transform.right;
        }
        if (Input.GetKey(KeyCode.D) == true)
        {
            transform.position += transform.right;
        }
    }
}
